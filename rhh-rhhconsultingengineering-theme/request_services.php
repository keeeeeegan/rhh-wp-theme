<?php
/**
 * @package WordPress
 * @subpackage RHH
 * Template Name: Request Services Form
 */
get_header(); ?>

<script type="text/javascript">

jQuery(document).ready(function(){

	function isNotEmpty(string) {
		if (string == "") {
			return false;
		}
		else {
			return true;
		}
	}

	function checkEmail(email) {
		var filter = /^([a-zA-Z0-9_.-])+@(([a-zA-Z0-9-])+.)+([a-zA-Z0-9]{2,4})+$/;
		if (!filter.test(email)) {
			return false;
		}
		return true;
	}

	jQuery('#submit').click(function(event) {
		var foundError = false;
		if (!isNotEmpty(jQuery('#name').val()))
		{
			jQuery('#invalid-name').show();
                        jQuery('#name').addClass('invalid_input');
			foundError = true;
		}
                else {
                        jQuery('#name').removeClass('invalid_input');
                        jQuery('#invalid-name').hide();
                }
		if (!checkEmail(jQuery('#email').val()))
		{
                        jQuery('#email').addClass('invalid_input');
			jQuery('#invalid-email').show();
			foundError = true;
		}
                else {
                        jQuery('#email').removeClass('invalid_input');
                        jQuery('#invalid-email').hide();
                }

		if (!foundError) {
			jQuery('#request_form').submit();
		}
                else {
                        event.preventDefault();
                }
                
	});

});
</script>

<div class="content">

<?php /* edit_post_link('Edit this page.', '<p class="edit_page">', '</p>'); */ ?>

<?php

if ($_GET['form'] == "sent") {
echo "<p id=\"success_message\">Request form sent successfully. Thank you.</p>";
}
?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entry">
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>       
         
<form action="<?php echo get_bloginfo(template_url); ?>/email_request.php" method="post" id="request_form" name="request_form">
<ul>
    <li><label for="name">Name: </label>							<input type="text" name="name" id="name" /></li>
    <p id="invalid-name" class="invalid_form_message">Please enter your name.</p></li>
    <li><label for="title">Title: </label>							<input type="text" name="title" id="title" /></li>
    <li><label for="address">Address: </label>						<input type="text" name="address" id="address" /></li>
    <li><label for="city">City: </label>							<input type="text" name="city" id="city" /></li>
    <li><label for="zip">Zip Code: </label>							<input type="text" name="zip" id="zip" /></li>
    <li><label for="state">State: </label>
		<select name="state" id="state">
			<option value="" selected="selected">Select a State...</option> 
			<option value="AL">Alabama</option> 
			<option value="AK">Alaska</option> 
			<option value="AZ">Arizona</option> 
			<option value="AR">Arkansas</option> 
			<option value="CA">California</option> 
			<option value="CO">Colorado</option> 
			<option value="CT">Connecticut</option> 
			<option value="DE">Delaware</option> 
			<option value="DC">District Of Columbia</option> 
			<option value="FL">Florida</option> 
			<option value="GA">Georgia</option> 
			<option value="HI">Hawaii</option> 
			<option value="ID">Idaho</option> 
			<option value="IL">Illinois</option> 
			<option value="IN">Indiana</option> 
			<option value="IA">Iowa</option> 
			<option value="KS">Kansas</option> 
			<option value="KY">Kentucky</option> 
			<option value="LA">Louisiana</option> 
			<option value="ME">Maine</option> 
			<option value="MD">Maryland</option> 
			<option value="MA">Massachusetts</option> 
			<option value="MI">Michigan</option> 
			<option value="MN">Minnesota</option> 
			<option value="MS">Mississippi</option> 
			<option value="MO">Missouri</option> 
			<option value="MT">Montana</option> 
			<option value="NE">Nebraska</option> 
			<option value="NV">Nevada</option> 
			<option value="NH">New Hampshire</option> 
			<option value="NJ">New Jersey</option> 
			<option value="NM">New Mexico</option> 
			<option value="NY">New York</option> 
			<option value="NC">North Carolina</option> 
			<option value="ND">North Dakota</option> 
			<option value="OH">Ohio</option> 
			<option value="OK">Oklahoma</option> 
			<option value="OR">Oregon</option> 
			<option value="PA">Pennsylvania</option> 
			<option value="RI">Rhode Island</option> 
			<option value="SC">South Carolina</option> 
			<option value="SD">South Dakota</option> 
			<option value="TN">Tennessee</option> 
			<option value="TX">Texas</option> 
			<option value="UT">Utah</option> 
			<option value="VT">Vermont</option> 
			<option value="VA">Virginia</option> 
			<option value="WA">Washington</option> 
			<option value="WV">West Virginia</option> 
			<option value="WI">Wisconsin</option> 
			<option value="WY">Wyoming</option>
		</select>

    <li><label for="email">Email: </label> <input type="text" name="email" id="email" />
    <p id="invalid-email" class="invalid_form_message">Please enter a valid email address.</p></li>
    <li><label for="phone">Phone: </label> <input type="text" name="phone" id="phone" /></li>
    <li><label for="service">Nature of service needed:</label>	<textarea name="service" id="service"></textarea></li>
</ul>
<div class="centered">
<input type="submit" value="Submit" id="submit" />                   
</div>

</form>            
            
            </div>
		</div>
		<?php endwhile; endif; ?>
		
</div>


<?php get_footer(); ?>
