<?php if ($_POST['name'] == '' || $_POST['email'] == '') {
         header("Location: /request-services/");
}
else {

$name = "";
$title = "";
$address = "";
$city = "";
$zip = "";
$state = "";
$email = "";
$phone = "";
$service = "";

if ($_POST['name']) {
	$name = addslashes($_POST['name']);
}
if ($_POST['title']) {
	$title = addslashes($_POST['title']);
}
if ($_POST['address']) {
	$address = addslashes($_POST['address']);
}
if ($_POST['city']) {
	$city = addslashes($_POST['city']);
}
if ($_POST['zip']) {
	$zip = addslashes($_POST['zip']);
}
if ($_POST['state']) {
	$state = addslashes($_POST['state']);
}
if ($_POST['email']) {
	$email = addslashes($_POST['email']);
}
if ($_POST['phone']) {
	$phone = addslashes($_POST['phone']);
}
if ($_POST['service']) {
	$service = addslashes($_POST['service']);
}

$to = "rhh@rhhconsultingengineering.com"; // CHANGE EMAIL HERE
$subject = "Request Services Form Submitted";
$message = "The following was submitted from the Request Services form on ".date("F j, Y, g:i a")."\n\n\n\nName: ".$name."\n\nTitle: ".$title."\n\nAddress: ".$address."\n\nCity: ".$city."\n\nZip: ".$zip."\n\nState: ".$state."\n\nEmail Address: ".$email."\n\nPhone Number: ".$phone."\n\nDescription of Request: ".$service;
$from = "request-services-form@rhhconsultingengineering.com";
$headers = "From: $from";
mail($to,$subject,$message,$headers);
header( 'Location: /request-services/?form=sent' );
}
?>
