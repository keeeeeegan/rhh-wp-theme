<?php
/**
 * @package WordPress
 * @subpackage RHH
 * Template Name: Project Page With Sidebar
 */
get_header(); ?>

<?php $post_data = get_post($post->post_parent, ARRAY_A);
$slug = $post_data['post_name'];?>

<div class="content <?php echo $slug ?>">

<?php /* edit_post_link('Edit this page.', '<p class="edit_page">', '</p>'); */ ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entry">

<div id="projects_sidebar">
<?php /*
<?php $permalink = get_permalink($post->post_parent); ?>
<a href="<?php echo $permalink; ?>">View our other projects:</a>
*/ ?>

See our other projects:

<?php

$children = wp_list_pages('title_li=&child_of='.$post->post_parent.'&echo=0');

if ($children)
{
?>
   <ul>
<?php
      echo $children;
?>
   </ul>
<?php
}
?>

</div>

			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>      	

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			</div>
		</div>
		<?php endwhile; endif; ?>
<div class="clear"></div>		
</div>


<?php get_footer(); ?>
