<?php
/**
 * @package WordPress
 * @subpackage RHH
 */

 $blog_url = get_bloginfo(wpurl);
?>

<div class="foot">
    <hr />
	<div class="nav">
		<ul>
			<li><a href="<?php echo $blog_url ?>/about-us/">About Us</a></li>
			<li><a href="<?php echo $blog_url ?>/solutions/">Solutions</a></li>
			<li><a href="<?php echo $blog_url ?>/services/">Services</a></li>
			<li><a href="<?php echo $blog_url ?>/disaster-assistance/">Disaster Assistance</a></li>
			<li><a href="<?php echo $blog_url ?>/manufacturers/">Manufacturers</a></li>
			<li><a href="<?php echo $blog_url ?>/completed-projects/">Completed Projects</a></li>
			<li><a href="<?php echo $blog_url ?>/request-services/">Request Services</a></li>
			<li><a href="<?php echo $blog_url ?>/contact-us/">Contact Us</a></li>
		</ul>
    </div>

    <div class="copyright">Copyright 2008-<?php echo date('Y'); ?>. RHH Consulting Group Inc. All Rights Reserved.</div>
    <div class="bot"></div>
</div>
<!-- //

Site powered by WordPress.
Template created by Keegan Berry, based on previous template.
http://keeganberry.com

W3C Valid code(-ish):
http://validator.w3.org/check?uri=http%3A%2F%2Frhhconsultingengineering.com%2F (There are 4 Google Analytics errors)

// -->
<?php wp_footer(); ?>
</body>
</html>