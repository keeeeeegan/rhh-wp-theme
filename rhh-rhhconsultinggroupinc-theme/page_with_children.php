<?php
/**
 * @package WordPress
 * @subpackage RHH
 * Template Name: Projects List Page
 */
get_header(); ?>

<div class="content">

<?php edit_post_link('Edit this page.', '<p class="edit_page">', '</p>'); ?>

		<?php if (have_posts()) : while (have_posts()) : the_post(); ?>
		<div class="post" id="post-<?php the_ID(); ?>">
		<h2><?php the_title(); ?></h2>
			<div class="entry">
			<?php the_content('<p class="serif">Read the rest of this page &raquo;</p>'); ?>      	

				<?php wp_link_pages(array('before' => '<p><strong>Pages:</strong> ', 'after' => '</p>', 'next_or_number' => 'number')); ?>
			</div>
<?php
$children = wp_list_pages('title_li=&child_of='.$post->ID.'&echo=0');

if ($children)
{
?>
   <ul>
<?php
      echo $children;
?>
   </ul>
<?php
}
?>

		</div>
		<?php endwhile; endif; ?>
<div class="clear"></div>		
</div>


<?php get_footer(); ?>
