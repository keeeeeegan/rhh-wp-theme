<!DOCTYPE html
PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd"> 
<?php
/**
 * @package WordPress
 * @subpackage RHH
 */

 $blog_url = get_bloginfo(wpurl);
?>

<html xmlns="http://www.w3.org/1999/xhtml" <?php language_attributes(); ?>>
 
<head> 
	<meta http-equiv="Content-Type" content="<?php bloginfo('html_type'); ?>; charset=<?php bloginfo('charset'); ?>" />    

<?php if (is_front_page()) {
print('
        <script type="text/javascript" src="'.get_bloginfo('template_url').'/js/swfobject.js"></script>
	<script type="text/javascript">
			var flashvars = {};
			flashvars.mp3 = "'.get_bloginfo('template_url').'/audio/intro.mp3";
			flashvars.autoplay = "1";
			var params = {};
			params.bgcolor = "004834";
			params.movie = "'.get_bloginfo('template_url').'/audio/player_mp3_mini.swf";
			var attributes = {};
			swfobject.embedSWF("'.get_bloginfo('template_url').'/audio/player_mp3_mini.swf", "mp3_audio_player", "155", "17", "9.0.0", false, flashvars, params, attributes);
	</script>
');
}
?>
	
	<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
	<link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    
	<title><?php wp_title('&laquo;', true, 'right'); ?> <?php bloginfo('name'); ?></title>

        <?php wp_enqueue_script("jquery"); ?>

	<?php wp_head(); ?>

<?php if (is_front_page()) {
print('
<script type="text/javascript">
jQuery(document).ready(function(){
');
}
if (is_front_page()) {
     if ($_COOKIE["mute_intro"] == "1") {
	echo "hide_audio();";
     }
     else if ($_COOKIE["mute_intro"] == "0") {
	echo "show_audio();";
     }
     else {
        echo "show_audio();";
     }
}
?>
<?php if (is_front_page()) {
print('
	function show_audio() {
                jQuery("#mp3_audio_player").show();
		jQuery("#audio_hide").html("mute introduction");
		jQuery("#audio_hide").removeClass("hidden");		
	}

	function hide_audio() {
                jQuery("#mp3_audio_player").hide();
		jQuery("#audio_hide").html("play introduction");
		jQuery("#audio_hide").addClass("hidden");
	}

	jQuery("#audio_hide").click(function(event){
		event.stopPropagation();
		event.preventDefault();
		if (jQuery("#mp3_audio_player").length > 0)
		{
			if (jQuery("#audio_hide").html() == "mute introduction") {
                             var currentTime = new Date();
                             var month = currentTime.getMonth();
                             var day = currentTime.getDate();
                             var year = currentTime.getFullYear()+1;
				document.cookie = "mute_intro=1; expires="+month+"/"+day+"/"+year+"; path=/";
				hide_audio();
			}
			else {
				document.cookie = "mute_intro=0; expires="+month+"/"+day+"/"+year+"; path=/";
				show_audio();
			}
		}
	});

});
</script>
');} ?>


<!--- // GOOGLE ANALYTICS CODE // --->

<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-19520623-1']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<!--- // END GOOGLE ANALYTICS CODE // --->



</head>
<body <?php body_class(); ?>>

<div class="head">

<?php
if (is_front_page()) {
print ('<div id="audio_menu">
<div id="mp3_audio_player">download flash</div>
<a href="#" id="audio_hide">');

		if ($_COOKIE["mute_intro"] == "1") {
			echo "play introduction";
		}
		else {
			echo "mute introduction";
		}

print ('</a>
</div>');
}
?>

	<h1 id="logo"><a href="<?php echo get_option('home'); ?>/"><?php bloginfo('name'); ?></a></h1>
	<!--<div class="description"><?php bloginfo('description'); ?></div>-->

	<div class="top"></div>
	
	<div class="nav">
		<ul>
			<li><a href="<?php echo $blog_url ?>/about-us/">About Us</a></li>
			<li><a href="<?php echo $blog_url ?>/solutions/">Solutions</a></li>
			<li><a href="<?php echo $blog_url ?>/services/">Services</a></li>
			<li><a href="<?php echo $blog_url ?>/disaster-assistance/">Disaster Assistance</a></li>
			<li><a href="<?php echo $blog_url ?>/manufacturers/">Manufacturers</a></li>
			<li><a href="<?php echo $blog_url ?>/completed-projects/">Completed Projects</a></li>
			<li><a href="<?php echo $blog_url ?>/request-services/">Request Services</a></li>
			<li><a href="<?php echo $blog_url ?>/contact-us/">Contact Us</a></li>
		</ul>	
	</div>
</div>
